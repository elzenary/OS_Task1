#include"STD_TYPES.h"
#include"BIT_MATH.h"
#include"timer0_register.h"
#include"Timer0_int.h"
#include"GIE_int.h"
#include"DIO_int.h"
#include "sevenSegment_int.h"
#include"DIO_register.h"
#include"delay.h"
#include"KEYPAD_int.h"
#include"LED_int.h"
#include"SWITCH_int.h"
volatile static  u8 ledCounterGlobal=1;
static volatile u8 ledCounterLOC=0;
volatile static  u8 welcome=0;
volatile static u8 endWelcomeFlag=0;
volatile static  u8 switchCounter=0;
volatile static  u8 led2Flag;
volatile static  u8 led2Counter;
volatile extern u32 LED_counter;



void taskWelcome(void)
{

	switch (welcome)
	{
	case 0:
		SevenSegment_voidWrite(11);
		welcome++;
		break;

	case 1:
		SevenSegment_voidWrite(16);
		welcome++;
		break;

	case 2:
		SevenSegment_voidWrite(11);
		welcome++;
		break;

	case 3:
		SevenSegment_voidWrite(16);
		welcome++;
		break;

	case 4:
		endWelcomeFlag=1;
		//welcome;
		break;

	default:
		break;
	}

}

void taskLed(void)
{

if(		ledCounterLOC==0&&
		ledCounterGlobal==1)
{
	//DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_HIGH);
	LED_voidSetLedOn(LED_u8_LED1);
}

	if(ledCounterLOC<ledCounterGlobal)
	{
		ledCounterLOC++;
	}
	else
	{

		if(DIO_voidGetPinValue( DIO_u8_PIN_5)==DIO_u8_LOW )
			//DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_HIGH);
			LED_voidSetLedOn(LED_u8_LED1);
		else
			//DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_LOW);
			LED_voidSetLedOff(LED_u8_LED1);

		ledCounterGlobal++;
		ledCounterLOC=0;
	}

	if(ledCounterGlobal==7)
	{
		//reset led1 sequence
		LED_counter=999;
		ledCounterLOC=0;
		ledCounterGlobal=1;
		//DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_HIGH);
		LED_voidSetLedOn(LED_u8_LED1);

	}
}





void taskSwitch(void)
{

	if(GetSwitchState(SWITCH_u8_SW1)==SWITCH_u8Pushed	)
	{
		switchCounter++;
	}
	else
	{
		switchCounter=0;
	}

	if(switchCounter==20)
	{
		led2Flag=1;
		led2Counter=0;
	}

	if(led2Flag==1)
	{
		//29 omn
		//DIO_voidSetPinValue( DIO_u8_PIN_4,DIO_u8_HIGH);
		LED_voidSetLedOn(LED_u8_LED2);
		led2Counter++;
		if(led2Counter==20)
		{

			//DIO_voidSetPinValue( DIO_u8_PIN_4,DIO_u8_LOW);
			 LED_voidSetLedOff(LED_u8_LED2);
			led2Flag=0;
			//reset led1 sequence
			ledCounterLOC=0;
			ledCounterGlobal=1;
			//DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_HIGH);
			LED_voidSetLedOn(LED_u8_LED1);
		}

	}



}





void taskKeypad(void)
{
	u8 ans,i;
	u16 res;
	if(endWelcomeFlag==1)
	{
		res=KEYPAD_u16GetStatus();
		for(i=0;i<16;i++)
		{
			if(GET_BIT(res,i)==1)
			{
				ans=i;


				break;
			}
			else
			{
				ans=16;
			}

		}
		switch(ans)
		{
		case 2:
			SevenSegment_voidWrite(0);
			break;
		case 15:
			SevenSegment_voidWrite(1);
			break;
		case 14 :
			SevenSegment_voidWrite(2);
			break;
		case 13:
			SevenSegment_voidWrite(3);
			break;
		case 11:
			SevenSegment_voidWrite(4);
			break;
		case 10:
			SevenSegment_voidWrite(5);
			break;
		case 9:
			SevenSegment_voidWrite(6);
			break;
		case 7:
			SevenSegment_voidWrite(7);
			break;
		case 6:
			SevenSegment_voidWrite(8);
			break;
		case 5:
			SevenSegment_voidWrite(9);
			break;


		default:
			SevenSegment_voidWrite(16);
			break;

		}
		//SevenSegment_voidWrite(ans);
	}
}




void main(void)
{

	DIO_voidInitialize();
	SevenSegment_voidInit();

	SevenSegment_voidWrite(16);

	//activate inter pull up for switch
	DIO_voidSetPinValue( DIO_u8_PIN_3,DIO_u8_HIGH);

	DIO_voidSetPinValue( DIO_u8_PIN_5,DIO_u8_LOW);

	/*pull up for keypad*/
	DIO_voidSetPinValue(DIO_u8_PIN_12,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_13,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_0,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_1,DIO_u8_HIGH);

	GIE_voidSetGIE();
	Timer0_init();
	while(1)
	{
		scheduler();
	}
}

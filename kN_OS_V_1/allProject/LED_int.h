#ifndef _LED_INT_H_
#define _LED_INT_H_

#include "LED_config.h"
/******************************************/
/* Description: This function is used to  */
/* 				set LED on 				  */
/******************************************/
u8 LED_voidSetLedOn(u8 u8LedIndexCpy);
/******************************************/
/* Description: This function is used to  */
/* 				set LED off 			  */
/******************************************/
u8 LED_voidSetLedOff(u8 u8LedIndexCpy);

#define u8Error 50
#endif




#ifdef _LED_INT_H_
/********************Public Configuration ******************************/
#ifndef _LED_Pub_CONFIG_H_
#define _LED_Pub_CONFIG_H_
/* Please add your LED index */
typedef enum{
	LED_u8_LED1,
	LED_u8_LED2,
	/*
	 * Please insert your LEDs here
	 */
	LED_u8_LED_NB
}LED_LEDIndex;
/***********************************************************************/
#endif

#endif

#ifdef _LED_PRIV_H_
/********************Private Configuration *****************************/
#ifndef _LED_PRI_CONFIG_H_
#define _LED_PRI_CONFIG_H_
/* Please write the pin index for each LED */
/* with the same order define in LED_LEDIndex */
static u8 LED_u8_PINS [LED_u8_LED_NB] =
{
		DIO_u8_PIN_5,
		DIO_u8_PIN_4

};

/* Please write the state for each switch */
/* with the same order define in LED_LEDIndex */
/* Knowing either LED_u8_Normal or LED_u8_REVERSE*/
u8 LED_u8_MODE [LED_u8_LED_NB] =
{
		LED_u8_NORMAL,
		LED_u8_NORMAL

};
/**********************************************************************/
#endif



#endif

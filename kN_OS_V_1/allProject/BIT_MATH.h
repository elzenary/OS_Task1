#ifndef BIT_MATH_H
#define BIT_MATH_H

#define SET_BIT(x,bitNum) (x)|=(1<<(bitNum))
#define CLR_BIT(x,bitNum) (x)&=~((1<<bitNum))
#define TOG_BIT(x,bitNum) (x)^=(1<<(bitNum))
#define GET_BIT(x,bitNum) ((x)&(1<<(bitNum)) )  >>(bitNum)

#define BIT_CONC(B0,B1,B2,B3,B4,B5,B6,B7) CONC_HELP(B0,B1,B2,B3,B4,B5,B6,B7)
#define CONC_HELP(B0,B1,B2,B3,B4,B5,B6,B7) 0b##B7##B6##B5##B4##B3##B2##B1##B0

#define BIT_CONC_6BITS(B0,B1,B2,B3,B4,B5) CONC_HELP_6BITS(B0,B1,B2,B3,B4,B5)
#define CONC_HELP_6BITS(B0,B1,B2,B3,B4,B5) 0b##B5##B4##B3##B2##B1##B0

/* Size of Byte which is equal to char size which is 8 bits */
#define BYTE_SIZE 	8

/* perform left circular shift on an input variable which shifts equal to Number Of Shifts variable */
#define LEFT_CIRCULAR_SHIFT(Var,NumberOfShifts) ((Var << NumberOfShifts) | (Var >> (sizeof(Var)*BYTE_SIZE - NumberOfShifts)))

/* perform Right circular shift on an input variable which shifts equal to Number Of Shifts variable */
#define RIGHT_CIRCULAR_SHIFT(Var,NumberOfShifts) ((Var >> NumberOfShifts) | (Var << (sizeof(Var)*BYTE_SIZE - NumberOfShifts)))


#endif

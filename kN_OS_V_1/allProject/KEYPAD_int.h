/*
 * KEYPAD_int.h
 *
 *  Created on: Jan 31, 2018
 *      Author: ahmedRaafat
 *      version:01
 */


#ifndef _KEYPAD_INT_H
#define _KEYPAD_INT_H

/*Description: this function returns the keypad switches*/
/*				each switch has a dedicated pin*/

u16 KEYPAD_u16GetStatus(void);

#endif

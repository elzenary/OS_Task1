/*
 * Switch_prog.c
 *
 *  Created on: Jan 22, 2018
 *      Author: MosEm
 */

#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "DELAY.h"

#include "DIO_int.h"

#include "SWITCH_priv.h"
#include "SWITCH_int.h"



u8 GetSwitchState(u8 u8SwitchIndexCpy)
{
	u8 u8Result;
	/*	Check if the input index  is in the range or not */
	if(u8SwitchIndexCpy<SWITCH_u8_SW_NB)
	{
		/*get the input pin value of switch*/
		u8Result = DIO_voidGetPinValue(SWITCH_u8SwitchToDioLink[u8SwitchIndexCpy]);
		/*check the state */
		if(u8Result == SWITCH_u8SwitchOpenState[u8SwitchIndexCpy])
		{
			u8Result = SWITCH_u8Released;
		}
		else
		{
			u8Result = SWITCH_u8Pushed;
		}
	}
	/*Error */
	else
	{
		u8Result =  SWITCH_u8Error;
	}

	return u8Result;
}


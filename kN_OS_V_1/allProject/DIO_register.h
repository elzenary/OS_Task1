#ifndef DIO_REGISTER_H
#define DIO_REGISTER_H

/*GROUP B Register*/
#define PORTB 	*((u8*)0x25)
#define DDRB 	*((u8*)0x24)
#define PINB 	*((volatile u8*)0x23)


/*GROUP C Register*/
#define PORTC 	*((u8*)0x28)
#define DDRC 	*((u8*)0x27)
#define PINC 	*((volatile u8*)0x26)


/*GROUP D Register*/
#define PORTD 	*((u8*)0x2B)
#define DDRD 	*((u8*)0x2A)
#define PIND 	*((volatile u8*)0x29)

#endif

#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_int.h"

#include "LED_int.h"
#include "LED_priv.h"

u8 LED_voidSetLedOn(u8 u8LedIndexCpy)
{
	u8 u8Result;

	if(u8LedIndexCpy<LED_u8_LED_NB)
	{
		if( LED_u8_MODE[u8LedIndexCpy] == LED_u8_NORMAL)
			DIO_voidSetPinValue(LED_u8_PINS[u8LedIndexCpy],DIO_u8_HIGH);
		else
			DIO_voidSetPinValue(LED_u8_PINS[u8LedIndexCpy],DIO_u8_LOW);
	}
	/*Error */
	else
	{
		u8Result = u8Error;
	}

	return u8Result;
}

u8 LED_voidSetLedOff(u8 u8LedIndexCpy)
{
	u8 u8Result;

		if(u8LedIndexCpy<LED_u8_LED_NB)
		{
			if( LED_u8_MODE[u8LedIndexCpy] == LED_u8_NORMAL)
				DIO_voidSetPinValue(LED_u8_PINS[u8LedIndexCpy],DIO_u8_LOW);
			else
				DIO_voidSetPinValue(LED_u8_PINS[u8LedIndexCpy],DIO_u8_HIGH);
		}
		/*Error */
		else
		{
			u8Result = u8Error;
		}

		return u8Result;
}


/*

 * KEYPAD_prog.c
 *
 *  Created on: Jan 31, 2018
 *      Author: ahmedRaafat
 */


#include"BIT_MATH.h"
#include"STD_TYPES.h"
#include"DIO_int.h"
#include"KEYPAD_conf.h"
#include"KEYPAFD_priv.h"
#include"KEYPAD_int.h"

static	u16 u16ResultLoc;
/*Description: this function returns the keypad switches*/
/*				each switch has a dedicated pin*/

u16 KEYPAD_u16GetStatus(void)
{
/*clear previous status*/
	u16ResultLoc=0;


	/*phase1*/
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_LOW);
	 DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
	 DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
	 DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
	 voidSetSwResult(0);
	 /*phase2*/
	 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
	 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_LOW);
	 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
	 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
	 	 voidSetSwResult(1);

		 /*phase3*/
		 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
		 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
		 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_LOW);
		 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
		 	 voidSetSwResult(2);

			 /*phase4*/
			 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
			 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
			 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
			 	 DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_LOW);
			 	 voidSetSwResult(3);


	 return u16ResultLoc;
}


static void voidSetSwResult(u8 u8CoulmnCPY)
{


		 if(DIO_voidGetPinValue(KEYPAD_u8_INP_1)== DIO_u8_LOW)
		 {
			 SET_BIT(u16ResultLoc,u8CoulmnCPY);
		 }
		 if(DIO_voidGetPinValue(KEYPAD_u8_INP_2)== DIO_u8_LOW)
		 {
			 SET_BIT(u16ResultLoc,u8CoulmnCPY+4);
		 }
		 if(DIO_voidGetPinValue(KEYPAD_u8_INP_3)== DIO_u8_LOW)
		 {
			 SET_BIT(u16ResultLoc,u8CoulmnCPY+8);
		 }
		 if(DIO_voidGetPinValue(KEYPAD_u8_INP_4)== DIO_u8_LOW)
		 {
			 SET_BIT(u16ResultLoc,u8CoulmnCPY+12);
		 }


}


#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_register.h"
#include "DIO_config.h"
#include "DIO_priv.h"
#include "DIO_int.h"

void DIO_voidSetPinValue(u8 u8PinIndexCPY,u8  u8PinValueCPY)
{
	if((u8PinIndexCPY>=DIO_u8_PORTB_START)&&(u8PinIndexCPY<=DIO_u8_PORTB_END))
	{
		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTB, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTB, u8PinIndexCPY);
		}
	}

	else if((u8PinIndexCPY>=DIO_u8_PORTC_START)&&(u8PinIndexCPY<=DIO_u8_PORTC_END))
	{
		u8PinIndexCPY-=(DIO_U8_PORTB_SIZE);

		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTC, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTC, u8PinIndexCPY);
		}
	}
	else if((u8PinIndexCPY>=DIO_u8_PORTD_START)&&(u8PinIndexCPY<=DIO_u8_PORTD_END))
	{
		u8PinIndexCPY-=(DIO_U8_PORTB_SIZE+DIO_U8_PORTC_SIZE);

		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTD, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTD, u8PinIndexCPY);
		}
	}
	else
	{

	}
}


/* Get Pin Value from MicroProcessor */
u8 DIO_voidGetPinValue(u8 u8PinIndexCPY)
{
	u8 u8Result=0;

	if((u8PinIndexCPY>=DIO_u8_PORTB_START)&&(u8PinIndexCPY<=DIO_u8_PORTB_END))
	{
		if( GET_BIT(PINB,  u8PinIndexCPY) == DIO_u8_HIGH)
		{
			u8Result= DIO_u8_HIGH;
		}
		else
		{
			u8Result= DIO_u8_LOW;
		}
	}
	else if((u8PinIndexCPY>=DIO_u8_PORTC_START)&&(u8PinIndexCPY<=DIO_u8_PORTC_END))
	{
		u8PinIndexCPY-=(DIO_U8_PORTB_SIZE);

		if( GET_BIT(PINC, u8PinIndexCPY)==DIO_u8_HIGH)
		{
			u8Result= DIO_u8_HIGH;
		}
		else
		{
			u8Result= DIO_u8_LOW;
		}
	}

	else if((u8PinIndexCPY>=DIO_u8_PORTD_START)&&(u8PinIndexCPY<=DIO_u8_PORTD_END))
	{
		u8PinIndexCPY-=(DIO_U8_PORTB_SIZE+DIO_U8_PORTC_SIZE);

		if( GET_BIT(PIND, u8PinIndexCPY)==DIO_u8_HIGH)
		{
			u8Result= DIO_u8_HIGH;
		}
		else
		{
			u8Result= DIO_u8_LOW;
		}
	}
	else
	{

	}

	return u8Result;
}


/*initialization function*/
void DIO_voidInitialize(void)

{
	DDRB=DIO_U8_PORTB_DIRECTION;
	DDRC=DIO_U8_PORTC_DIRECTION;
	DDRD=DIO_U8_PORTD_DIRECTION;
}

#ifndef _DIO_CONFIG_H
/*ctrl h for replace PIN_DIR_ PIN_DIR_DIR*/
/*alt copy vertical*/

#define DIO_CONFIG_H
/*unused pins input of output*/
#define DIO_u8INPUT 0
#define DIO_u8OUTPUT 1

/*
  Connections section :



1.    On which pin is LED1 connected to in Atmega328P/Atmega2560?

=> PB5 pin 13   DIO_u8_PIN_5

2.    On which pin is LED2 connected to in Atmega328P/Atmega2560?

=> PB4 pin 12   DIO_u8_PIN_4

3.    On which pin is the button connected to in Atmega328P/Atmega2560?

=> PB3 pin 11   DIO_u8_PIN_3

4.    On which pins are the keypad connected to in Atmega328P/Atmega2560?

=> Port C, pins 0 to 5
A0 to A5
B0,1 pin8 9

5.    On which pins 7seg pins (a,b,c,d,e,f,g,dot,common) are connected to in Atmega328P/Atmega2560?

=> Port D, pins 0 to 7 respectively pin 0 to 7    enableB2 10
*/
#define DIO_u8_PIN_DIR_0  	DIO_u8INPUT
#define DIO_u8_PIN_DIR_1   	DIO_u8INPUT
#define DIO_u8_PIN_DIR_2   	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_3  	DIO_u8INPUT
#define DIO_u8_PIN_DIR_4  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_5  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_6  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_7  	DIO_u8OUTPUT

#define DIO_u8_PIN_DIR_8    DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_9    DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_10   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_11   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_12   DIO_u8INPUT
#define DIO_u8_PIN_DIR_13   DIO_u8INPUT

#define DIO_u8_PIN_DIR_14   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_15   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_16  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_17  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_18   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_19   DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_20  	DIO_u8OUTPUT
#define DIO_u8_PIN_DIR_21  	DIO_u8OUTPUT


#endif

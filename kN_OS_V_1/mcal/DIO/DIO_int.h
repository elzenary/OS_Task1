/*interface file will be constand for different targets */
#ifndef DIO_INT_H
#define DIO_INT_H



void DIO_voidInitialize(void);

void DIO_voidSetPinValue(u8 u8PinIndexCPY,u8  u8PinValueCPY);

u8 DIO_voidGetPinValue(u8 u8PinIndexCPY);

/*specify index macro for high and low */
#define DIO_u8_HIGH 	1
#define DIO_u8_LOW  	0

/*define pin names constant for different targets*/

/* PORT B */
#define DIO_u8_PIN_0 	0
#define DIO_u8_PIN_1  	1
#define DIO_u8_PIN_2  	2
#define DIO_u8_PIN_3  	3
#define DIO_u8_PIN_4 	4
#define DIO_u8_PIN_5 	5
#define DIO_u8_PIN_6 	6
#define DIO_u8_PIN_7 	7

/* PORT C */
#define DIO_u8_PIN_8 	8
#define DIO_u8_PIN_9 	9
#define DIO_u8_PIN_10 	10
#define DIO_u8_PIN_11 	11
#define DIO_u8_PIN_12 	12
#define DIO_u8_PIN_13 	13

/* PORT D */
#define DIO_u8_PIN_14 	14
#define DIO_u8_PIN_15 	15
#define DIO_u8_PIN_16 	16
#define DIO_u8_PIN_17 	17
#define DIO_u8_PIN_18 	18
#define DIO_u8_PIN_19 	19
#define DIO_u8_PIN_20 	20
#define DIO_u8_PIN_21 	21




#endif

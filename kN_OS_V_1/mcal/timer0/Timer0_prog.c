
#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "Timer0_register.h"
#include "Timer0_config.h"
#include "Timer0_priv.h"
#include "Timer0_int.h"

#define NUM_OF_TASKS	4

typedef struct
{
	void (*ptr) (void);
	u16 periodicity;

}Task;

/*tasks prototypes*/
void taskWelcome(void);
void taskLed(void);
void taskSwitch(void);
void taskKeypad(void);

Task SysTasks[NUM_OF_TASKS] =
{
		{taskWelcome,1000},
		{taskLed,1000},
		{taskSwitch,100},
		{taskKeypad,1000},
};

static volatile u8 CTC_Flag;
static volatile u32 CTC_counter=0;
 volatile u32 LED_counter=0;


void scheduler(void)
{
	if(CTC_Flag == 1 )
	{
		CTC_Flag =0;
		if(CTC_counter%SysTasks[0].periodicity==0)
		{
			taskWelcome();
		}
		if(LED_counter%SysTasks[1].periodicity==0)
		{
			taskLed();
		}
		if(CTC_counter%SysTasks[2].periodicity==0)
		{
			taskSwitch();
		}
		if(CTC_counter%SysTasks[3].periodicity==0)
		{
			taskKeypad();
		}


	}
}


void Timer0_init(void)
{

	/*initialize TCNT*/
	TCNT0  = 0b00000000;

	/*clear flag*/
	SET_BIT(TIFR0,1);

	/*set ctc mode*/
	TCCR0A = 0b00000010;

	/*enable CTC compare A */
	SET_BIT(TIMSK0,1);

	/*initialize ocr0A*/
	OCR0A = 249;


	/*set prescaler*/
	TCCR0B = 0b00000011;


}


void __vector_14 (void) __attribute__((signal,used));
void __vector_14 (void)
{
	CTC_counter++;
	LED_counter++;
	CTC_Flag = 1;
}











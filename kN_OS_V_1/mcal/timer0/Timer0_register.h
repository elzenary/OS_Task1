#ifndef Timer0_REGISTER_H
#define Timer0_REGISTER_H


#define TCNT0 	*((volatile u8*)0x46)

#define TCCR0A 	*((volatile u8*)0x44)
#define TCCR0B  *((volatile u8*)0x45)

#define OCR0A   *((volatile u8*)0x47)
#define OCR0B   *((volatile u8*)0x48)

#define GTCCR 	*((volatile u8*)0x43)

#define TIMSK0 	*((volatile u8*)0x6E)
#define TIFR0  	*((volatile u8*)0x35)

#endif

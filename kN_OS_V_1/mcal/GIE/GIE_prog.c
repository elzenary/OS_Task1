
#include"STD_TYPES.h"
#include"BIT_MATH.h"
#include"GIE_reg.h"

void GIE_voidSetGIE(void){
	SET_BIT(SREG,7);
}

void GIE_voidClrGIE(void){
		CLR_BIT(SREG,7);
}
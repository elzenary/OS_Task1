
#ifndef GIE_INT_H
#define GIE_INT_H

void GIE_voidSetGIE(void);

void GIE_voidClrGIE(void);

#endif
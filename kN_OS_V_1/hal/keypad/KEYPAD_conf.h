/*
 * KEYPAD_conf.h
 *
 *  Created on: Jan 31, 2018
 *      Author: ahmedRaafat
 *      version:01
 */


#ifndef _KEYPAD_CONF_H
#define _KEYPAD_CONF_H

#define KEYPAD_u8_INP_1  DIO_u8_PIN_12
#define KEYPAD_u8_INP_2  DIO_u8_PIN_13
#define KEYPAD_u8_INP_3  DIO_u8_PIN_0
#define KEYPAD_u8_INP_4  DIO_u8_PIN_1

#define KEYPAD_u8_OUT_1  DIO_u8_PIN_8
#define KEYPAD_u8_OUT_2  DIO_u8_PIN_9
#define KEYPAD_u8_OUT_3  DIO_u8_PIN_10
#define KEYPAD_u8_OUT_4  DIO_u8_PIN_11

#endif

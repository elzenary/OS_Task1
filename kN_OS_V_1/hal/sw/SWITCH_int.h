/*
 * SWITCH_int.h
 *
 *  Created on: Jan 22, 2018
 *      Author: MosEm
 */

#ifndef SWITCH_INT_H_
#define SWITCH_INT_H_

#include "SWITCH_config.h"

/******************************************/
/* Description: This hash define is used  */
/* 				to set  the values of 	  */
/* 				pushed, released and 	  */
/* 				error values			  */
/******************************************/
#define SWITCH_u8Released  	1
#define SWITCH_u8Pushed		50
#define SWITCH_u8Error 		100

/******************************************/
/* Description: This function is used to  */
/* 				return the state of		  */
/*              switch pushed or released */
/******************************************/
u8 GetSwitchState(u8 u8SwitchIndexCpy);

#endif /* SWITCH_INT_H_ */

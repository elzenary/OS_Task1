/*
 * SWITCH_config.h
 *
 *  Created on: Jan 22, 2018
 *      Author: MosEm
 */

#ifndef SWITCH_CONFIG_H_
#define SWITCH_CONFIG_H_


#ifdef SWITCH_INT_H_
/********************Public Configuration ******************************/
/* Please add your switch index */
typedef enum{
	SWITCH_u8_SW1,

	/*
	 * Please insert your switches here
	 */
	SWITCH_u8_SW_NB
}SWITCH_SWITCHIndex;
/**********************************************************************/
#endif

#ifdef SWITCH_PRIV_H_
/********************Private Configuration *****************************/
/* Please write the pin index for each switch */
/* with the same order define in SWITCH_SWITCHIndex */
static u8 SWITCH_u8SwitchToDioLink [SWITCH_u8_SW_NB] =
{
		DIO_u8_PIN_3


};

/* Please write the state for each switch */
/* with the same order define in SWITCH_SWITCHIndex */
/* Knowing that open state is the released state */
u8 SWITCH_u8SwitchOpenState [SWITCH_u8_SW_NB] =
{
		DIO_u8_HIGH

};
#endif
/**********************************************************************/
#endif /* SWITCH_CONFIG_H_ */

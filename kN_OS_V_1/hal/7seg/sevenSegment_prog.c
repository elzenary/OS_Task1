/*
 * sevenSegment_prog.c
 *
 *  Created on: Jan 6, 2018
 *      Author: ahmedRaafat
 */

/*include the required header files*/

/*the standard types library*/
#include "STD_TYPES.h"
/*bit math library*/
#include "BIT_MATH.h"
/*DIO interface header file*/
#include "DIO_int.h"
/*the seven segment private header*/
#include "sevenSegment_priv.h"
/*the seven segment configuration file*/
#include "sevenSegment_config.h"
/*the seven segment interface file*/
#include "sevenSegment_int.h"

/*define global array that holds the values of the segment for different numbers*/

/*check if the seven segment mode is common cathode*/
#if SEVEN_SEGMENT_u8_MODE == SEVENSEGMENT_u8_COMMON_CATHODE
/*setup the appropriate lookup table for this mode*/
u8  SevenSegmentu8LookUpTable[ SEVENSEGMENT_u8_LOOKUPTABLE_SIZE]={0b00111111,0b00110000,0b01011011,0b01001111,0b01100110,0b01101101 ,0b01111101,0b00000111,0b01111111,0b01101111,0b11110111,0b11111111,0b00111001,0b10111111,0b01111001,0b01110001,0b00000000};
/*end the condition*/
#endif

/*check if the seven segment mode is common anode*/
#if SEVEN_SEGMENT_u8_MODE == SEVENSEGMENT_u8_COMMON_ANODE
/*setup the appropriate lookup table for this mode*/
u8 SevenSegmentu8LookUpTable[ SEVENSEGMENT_u8_LOOKUPTABLE_SIZE]={0b11000000,0b11001111,0b10100100,0b10110000,0b10011001,0b10010010 ,0b10000010,0b11111000,0b10000000,0b10010000,0b00001000,0b00000000,0b11000110,0b01000000,0b10000110,0b10001110,0b11111111};
/*end the condition*/
#endif

/*seven segment initialize function
 * takes void and return void
 * its tasks
 * initialize the direction of the DIO pins for A,B,C,D,E,F,G,DOT
 * initialize the direction of the common enable pin
 * put HIGH or LOW on the enable pin based on it is common anode or cathode
 */
void SevenSegment_voidInit(void)
/*the beginning of  SevenSegment_voidInit function*/
{
	/*initialize the DIO pins to set all seven segment pins as output*/
	DIO_voidInitialize();

	/*check if the seven segment mode is common anode*/
	#if SEVEN_SEGMENT_u8_MODE == SEVENSEGMENT_u8_COMMON_ANODE
	/*set the enable pin high if the type is common anode*/
	DIO_voidSetPinValue(SEVEN_SEGMENT_u8_COMMON_PIN,DIO_u8_HIGH);
	/*end the conditional preprocessor if*/
	#endif

	/*check if the seven segment mode is common anode*/
	#if SEVEN_SEGMENT_u8_MODE == SEVENSEGMENT_u8_COMMON_CATHODE
	/*set the enable pin low if the type is common cathode*/
	DIO_voidSetPinValue(SEVEN_SEGMENT_u8_COMMON_PIN,DIO_u8_LOW);
	/*end the conditional preprocessor if*/
	#endif

/*the end of  SevenSegment_voidInit function*/
}


/*write a certain value on the seven segment
 the function takes u8 and return void
 the input parameter should be value from 1 to 16 to be output to the 7 segment
 value from 10 to 15 will be equivalent to their hex values a,b,c,d,e,f
 */
void SevenSegment_voidWrite(u8 u8ValueCPY)
{
/*the beginning of SevenSegment_voidWrite function*/

	/*define local variable to hold the current processing bit value*/
	u8 u8BitValueLOC;

	/*update u8ValueCPY to be equal its real value from the lookup table*/
	u8ValueCPY=SevenSegmentu8LookUpTable[u8ValueCPY];

	/*get the  bit number 0 in the value you want to write on the seven segment*/
	u8BitValueLOC = GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_ZERO);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number a if bit value number zero equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_A_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number a if bit value number zero equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_A_PIN ,DIO_u8_LOW);
	}

	/*get the  bit number 1 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_ONE);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number b if bit value number one equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_B_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number b if bit value number one equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_B_PIN,DIO_u8_LOW);
	}

	/*get the  bit number 2 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_TWO);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number c if bit value number 2 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_C_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number c if bit value number 2 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_C_PIN ,DIO_u8_LOW);
	}

	/*get the  bit number 3 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_THREE);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number d if bit value number 3 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_D_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number d if bit value number 3 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_D_PIN ,DIO_u8_LOW);
	}

	/*get the  bit number 4 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_FOUR);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number e if bit value number 4 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_E_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number e if bit value number 4 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_E_PIN,DIO_u8_LOW);
	}

	/*get the  bit number 5 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_FIVE);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number f if bit value number 5 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_F_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number f if bit value number 5 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_F_PIN ,DIO_u8_LOW);
	}

	/*get the  bit number 6 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_SIX);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number g if bit value number 6 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_G_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number g if bit value number 6 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_G_PIN ,DIO_u8_LOW);
	}

	/*get the  bit number 7 in the value you want to write on the seven segment*/
	u8BitValueLOC=GET_BIT(u8ValueCPY,SEVENSEGMENT_u8_BIT_NUMBER_SEVEN);

	/*check this bit value*/
	if(u8BitValueLOC== SEVENSEGMEMENT_u8_BIT_VALUE_ONE)
	{
		/*write HIGH on pin number dot if bit value number 7 equal one*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_DOT_PIN,DIO_u8_HIGH);
	}
	else
	{
		/*write LOW on pin number dot if bit value number 7 equal zero*/
		DIO_voidSetPinValue(SEVEN_SEGMENT_u8_DOT_PIN ,DIO_u8_LOW);
	}

}

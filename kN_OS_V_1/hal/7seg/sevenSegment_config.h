/*
 * sevenSegment_config.h
 *
 *  Created on: Jan 6, 2018
 *      Author: ahmedRaafat
 */

/*header guards to protect from including the headers more than one time*/
#ifndef SEVEN_SEGMENT_CONFIG_H

/*define the module name for the first time*/
#define  SEVEN_SEGMENT_CONFIG_H
/*choose the 7 segment mode either in cathode or anode mode*/
/* un comment the mode you are working on*/

/*specify the seven segment mode as common cathode*/
#define  SEVEN_SEGMENT_u8_MODE SEVENSEGMENT_u8_COMMON_CATHODE

/*specify the 7 segment mode as common anode*/

/*#define SEVEN_SEGMENT_u8_MODE SEVENSEGMENT_u8_COMMON_ANODE*/


/*choose the DIO pin which is connected to the 7 segment pins*/
/*Knowing that the arrangement of the 7 segment pins as follow*/

/*
g  f  COM  a  b
_|__|___|__|__|__
|	  ____		|
|	| ____	|	|
|	| ____	|.	|
|_______________|
  |  |   |  |  |
 e  d   COM  c dot

*/

/*specify which DIO pin is connected to the seven segment a pin*/
#define SEVEN_SEGMENT_u8_A_PIN 		DIO_u8_PIN_14
/*specify which DIO pin is connected to the seven segment b pin*/
#define SEVEN_SEGMENT_u8_B_PIN 		DIO_u8_PIN_15
/*specify which DIO pin is connected to the seven segment c pin*/
#define SEVEN_SEGMENT_u8_C_PIN 		DIO_u8_PIN_16
/*specify which DIO pin is connected to the seven segment d pin*/
#define SEVEN_SEGMENT_u8_D_PIN 		DIO_u8_PIN_17
/*specify which DIO pin is connected to the seven segment e pin*/
#define SEVEN_SEGMENT_u8_E_PIN 		DIO_u8_PIN_18
/*specify which DIO pin is connected to the seven segment f pin*/
#define SEVEN_SEGMENT_u8_F_PIN 		DIO_u8_PIN_19
/*specify which DIO pin is connected to the seven segment g pin*/
#define SEVEN_SEGMENT_u8_G_PIN 		DIO_u8_PIN_20
/*specify which DIO pin is connected to the seven segment dot pin*/
#define SEVEN_SEGMENT_u8_DOT_PIN 	DIO_u8_PIN_21

/*specify the common enable control signal connected to seven segment COMM pin*/
#define SEVEN_SEGMENT_u8_COMMON_PIN  DIO_u8_PIN_2

/*note*/
/*you must specify the pins you chooses as output pins in the DIO driver configuration file*/

/*end the conditional PRE processor*/
#endif

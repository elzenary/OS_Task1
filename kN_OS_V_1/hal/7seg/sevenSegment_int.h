/*
 * sevenSegment_int.h
 *
 *  Created on: Jan 6, 2018
 *      Author: ahmedRaafat
 */

/*header guards to protect from including the headers more than one time*/
#ifndef SEVEN_SEGMENT_INT_H
/*define the SEVEN_SEGMENT_INT_H for the first time if it is not defined*/
#define SEVEN_SEGMENT_INT_H

/*note you shouldn't call the initialization function from DIO driver
 * as it will be called in SevenSegment_voidInit
 * you should put the appropriate values input output values in the DIO configuration files
 * also the right pin numbers in the seven segment configuration file*/

/*seven segment initialize function
 * takes void and return void
 * its tasks
 * initialize the direction of the DIO pins for A,B,C,D,E,F,G,DOT
 * initialize the direction of the common enable pin
 * put HIGH or LOW on the enable pin based on it is common anode or cathode
 */
void SevenSegment_voidInit(void);

/*write a certain value on the seven segment
 the function takes u8 and return void
 the input parameter should be value from 1 to 16 to be output to the 7 segment
 value from 10 to 15 will be equivalent to their hex values a,b,c,d,e,f
 */
void SevenSegment_voidWrite(u8 u8ValueCPY);

/*the end of the protected guard condition*/
#endif

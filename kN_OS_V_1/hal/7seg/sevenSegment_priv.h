/*
 * sevenSegment_priv.h
 *
 *  Created on: Jan 6, 2018
 *      Author: ahmedRaafat
 */

/*this file contains the private headers that will not be part of the interface file*/

/*Specify the header guards to protect multiple including to the file*/
#ifndef SEVEN_SEGMENT_PRIV_H

/*define the module name for the first time*/
#define  SEVEN_SEGMENT_PRIV_H

/* define index for the seven segment common anode mode from u8 type*/
#define SEVENSEGMENT_u8_COMMON_ANODE 		1
/* define index for the seven segment common cathode mode from u8 type*/
#define SEVENSEGMENT_u8_COMMON_CATHODE 		0

/*define the size of the lookup table array for the seven segment*/
#define SEVENSEGMENT_u8_LOOKUPTABLE_SIZE 17

/*define bit value 1 to be checked after that*/
#define SEVENSEGMEMENT_u8_BIT_VALUE_ONE 1

/*define bit value 0 to be checked after that*/
#define SEVENSEGMEMENT_u8_BIT_VALUE_ZERO 0

/*define different bits in one byte*/

/*define bit number 0*/
#define SEVENSEGMENT_u8_BIT_NUMBER_ZERO 0
/*define bit number 1*/
#define SEVENSEGMENT_u8_BIT_NUMBER_ONE 1
/*define bit number 2*/
#define SEVENSEGMENT_u8_BIT_NUMBER_TWO 2
/*define bit number 3*/
#define SEVENSEGMENT_u8_BIT_NUMBER_THREE 3
/*define bit number 4*/
#define SEVENSEGMENT_u8_BIT_NUMBER_FOUR 4
/*define bit number 5*/
#define SEVENSEGMENT_u8_BIT_NUMBER_FIVE 5
/*define bit number 6*/
#define SEVENSEGMENT_u8_BIT_NUMBER_SIX 6
/*define bit number 7*/
#define SEVENSEGMENT_u8_BIT_NUMBER_SEVEN 7


/*
 * note in the segments i have
 * AH common cathode
 * BS common Anode
 */

/*end the conditional PRE processor*/
#endif
